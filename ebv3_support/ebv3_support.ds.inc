<?php
/**
 * @file
 * ebv3_support.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function ebv3_support_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|ebv3_event|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'ebv3_event';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
        'empty_fields_handler' => '',
        'empty_fields_emptyfieldtext_empty_text' => '',
      ),
    ),
  );
  $export['node|ebv3_event|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|ebv3_event|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'ebv3_event';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
        'empty_fields_handler' => '',
        'empty_fields_emptyfieldtext_empty_text' => '',
      ),
    ),
  );
  $export['node|ebv3_event|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function ebv3_support_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|ebv3_event|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'ebv3_event';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_ebv3_event_image',
      ),
      'left' => array(
        1 => 'title',
        2 => 'field_ebv3_event_date',
        3 => 'body',
        4 => 'field_ebv3_event_url',
        5 => 'field_ebv3_event_category',
      ),
    ),
    'fields' => array(
      'field_ebv3_event_image' => 'header',
      'title' => 'left',
      'field_ebv3_event_date' => 'left',
      'body' => 'left',
      'field_ebv3_event_url' => 'left',
      'field_ebv3_event_category' => 'left',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|ebv3_event|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|ebv3_event|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'ebv3_event';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_ebv3_event_image',
        1 => 'title',
        2 => 'field_ebv3_event_date',
        3 => 'body',
        4 => 'field_ebv3_event_category',
      ),
    ),
    'fields' => array(
      'field_ebv3_event_image' => 'ds_content',
      'title' => 'ds_content',
      'field_ebv3_event_date' => 'ds_content',
      'body' => 'ds_content',
      'field_ebv3_event_category' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|ebv3_event|teaser'] = $ds_layout;

  return $export;
}
