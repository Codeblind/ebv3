<?php
/**
 * @file
 * ebv3_support.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ebv3_support_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ebv3_views_events_listing';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Eventbrite Events Listing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Submit';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Clear ';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_ebv3_event_date_value' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_ebv3_event_date_value2' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_ebv3_event_category_tid' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => 0,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'keys' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'full_html';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'events_calendar_listing';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no events that match your search criteria. Please widen your search, or select the Clear button to reset the results.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Eventbrite date */
  $handler->display->display_options['fields']['field_ebv3_event_date']['id'] = 'field_ebv3_event_date';
  $handler->display->display_options['fields']['field_ebv3_event_date']['table'] = 'field_data_field_ebv3_event_date';
  $handler->display->display_options['fields']['field_ebv3_event_date']['field'] = 'field_ebv3_event_date';
  $handler->display->display_options['fields']['field_ebv3_event_date']['label'] = '';
  $handler->display->display_options['fields']['field_ebv3_event_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ebv3_event_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
  );
  /* Field: Content: Eventbrite URL */
  $handler->display->display_options['fields']['field_ebv3_event_url']['id'] = 'field_ebv3_event_url';
  $handler->display->display_options['fields']['field_ebv3_event_url']['table'] = 'field_data_field_ebv3_event_url';
  $handler->display->display_options['fields']['field_ebv3_event_url']['field'] = 'field_ebv3_event_url';
  $handler->display->display_options['fields']['field_ebv3_event_url']['label'] = '';
  $handler->display->display_options['fields']['field_ebv3_event_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_ebv3_event_url']['alter']['text'] = '<a href="[field_ebv3_event_url-url]">Full Details and Ticket Information at Eventbrite.com</a>';
  $handler->display->display_options['fields']['field_ebv3_event_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ebv3_event_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_ebv3_event_url']['type'] = 'link_absolute';
  $handler->display->display_options['fields']['field_ebv3_event_url']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<br><br>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Eventbrite date -  start date (field_ebv3_event_date) */
  $handler->display->display_options['sorts']['field_ebv3_event_date_value']['id'] = 'field_ebv3_event_date_value';
  $handler->display->display_options['sorts']['field_ebv3_event_date_value']['table'] = 'field_data_field_ebv3_event_date';
  $handler->display->display_options['sorts']['field_ebv3_event_date_value']['field'] = 'field_ebv3_event_date_value';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'AND',
    3 => 'OR',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ebv3_event' => 'ebv3_event',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Eventbrite date -  start date (field_ebv3_event_date) */
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['id'] = 'field_ebv3_event_date_value';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['table'] = 'field_data_field_ebv3_event_date';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['field'] = 'field_ebv3_event_date_value';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['group'] = 2;
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['expose']['operator_id'] = 'field_ebv3_event_date_value_op';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['expose']['label'] = 'Start';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['expose']['operator'] = 'field_ebv3_event_date_value_op';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['expose']['identifier'] = 'start';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_ebv3_event_date_value']['default_date'] = 'now';
  /* Filter criterion: Content: Eventbrite date - end date (field_ebv3_event_date:value2) */
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['id'] = 'field_ebv3_event_date_value2';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['table'] = 'field_data_field_ebv3_event_date';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['field'] = 'field_ebv3_event_date_value2';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['operator'] = '<=';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['group'] = 2;
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['expose']['operator_id'] = 'field_ebv3_event_date_value2_op';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['expose']['label'] = 'End';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['expose']['operator'] = 'field_ebv3_event_date_value2_op';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['expose']['identifier'] = 'end';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_ebv3_event_date_value2']['default_date'] = 'now +30 day';
  /* Filter criterion: Content: Event Category (field_ebv3_event_category) */
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['id'] = 'field_ebv3_event_category_tid';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['table'] = 'field_data_field_ebv3_event_category';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['field'] = 'field_ebv3_event_category_tid';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['group'] = 3;
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['expose']['operator_id'] = 'field_ebv3_event_category_tid_op';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['expose']['label'] = 'Event Category';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['expose']['operator'] = 'field_ebv3_event_category_tid_op';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['expose']['identifier'] = 'category';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['vocabulary'] = 'event_category';
  $handler->display->display_options['filters']['field_ebv3_event_category_tid']['error_message'] = FALSE;
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'contains';
  $handler->display->display_options['filters']['combine']['group'] = 3;
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['label'] = 'Keyword Search';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['combine']['group_info']['label'] = 'Combine fields filter';
  $handler->display->display_options['filters']['combine']['group_info']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['combine']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );

  /* Display: Page: Events */
  $handler = $view->new_display('page', 'Page: Events', 'page');
  $handler->display->display_options['path'] = 'visit/events';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Calendar of Events';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'menu-secondary-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['ebv3_views_events_listing'] = array(
    t('Master'),
    t('Events'),
    t('more'),
    t('Submit'),
    t('Clear '),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Advanced options'),
    t('Select any filter and click on Apply to see results'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('There are no events that match your search criteria. Please widen your search, or select the Clear button to reset the results.'),
    t('<a href="[field_ebv3_event_url-url]">Full Details and Ticket Information at Eventbrite.com</a>'),
    t('<br><br>'),
    t('Start'),
    t('End'),
    t('Event Category'),
    t('Keyword Search'),
    t('Combine fields filter'),
    t('Page: Events'),
  );
  $export['ebv3_views_events_listing'] = $view;

  return $export;
}
