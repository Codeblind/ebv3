<?php
/**
 * @file
 * ebv3_support.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ebv3_support_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ebv3_support_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ebv3_support_node_info() {
  $items = array(
    'ebv3_event' => array(
      'name' => t('Eventbrite Event'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
