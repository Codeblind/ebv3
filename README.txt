Eventrbite API V3
=================

A Drupal 7 module for importing events and images from Eventbrite.com. 

This module is in a very rough, pre-alpha state and not intended for general consumption at this time.

Please contact via https://bitbucket.org/Codeblind/ if you would like to assist in the development of this project. 