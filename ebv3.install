<?php
/**
 * Created by PhpStorm.
 * User: eriks
 * Date: 6/19/15
 * Time: 2:39 PM
 */
function ebv3_schema() {
  $schema['ebv3_event'] = _ebv3_event_schema();
  return $schema;
}

/**
 * Contains schema definitions for the ebv3_event table.
 * @return mixed
 */
function _ebv3_event_schema() {
  return array(
    'description' => t('A maintenance table for Eventbrite V3 API.'),
    'fields' => array(
      'eid' => array(
        'description' => 'The event id as defined at Eventbrite.com',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'nid' => array(
        'description' => 'Node to which an EbV3 event maps.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'logoid' => array(
        'description' => 'The id assigned by Eventbrite.com to the event logo file.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'fid' => array(
        'description' => 'The file API id of the stored logo file.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'title' => array(
        'description' => 'The title of the mapped node, always treated a non-markup plain text.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the node was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the node was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'remote_changed' => array(
        'description' => 'Timestamp when event was last modified on Eventbrite.com.',
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE
      ),
    ),
    'indexes' => array(
      'changed' => array('changed'),
      'created' => array('created'),
    ),
    'unique keys' => array(
      'eid' => array('eid'),
      'nid' => array('nid')
    ),
    'primary key' => array('eid'),
  );
}

/**
 * Adds Eventbrite logo file tracking.
 */
function ebv3_update_7100() {

  // Add logoid column.
  $logoid = array(
    'description' => 'The id assigned by Eventbrite.com to the event logo file.',
    'type' => 'varchar',
    'length' => 50,
    'not null' => FALSE,
    'default' => ''
  );
  db_add_field( 'ebv3_event', 'logoid', $logoid);

  // Add fid column.
  $fid = array(
    'description' => 'The file API id of the stored logo file.',
    'type' => 'int',
    'not null' => FALSE,
  );
  db_add_field( 'ebv3_event', 'fid', $fid);
}