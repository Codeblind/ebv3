<?php

/**
 * Provides default methods for working with the Eventbrite API V3.
 *
 */
class Ebv3Api {

  protected $api_token;

  public function __construct() {

    $this->api_token = "";

    // This makes sure an API token is set before
    // this application is usable.
    $this->GetAPIToken();
  }

  /**
   * Puts the API Key into a global member.
   * If the API Key has not been set, redirects
   * to the administration page.
   */
  private function GetAPIToken() {
    $token = variable_get('ebv3_api_token', FALSE);
    if ($token) {
      $this->api_token = $token;
    }
    else {
      $msg = t('Please enter your Eventbrite.com API token.');
      drupal_set_message($msg, 'warning');
      drupal_goto('admin/eventbrite');
      exit();
    }
  }

  /**
   * A requesting agent that wraps all calls to Eventbrite.com.
   */
  /**
   * A requesting agent that wraps all calls to Eventbrite.com.
   */
  public function request($request, $options = array()) {
    $result = drupal_http_request($request, $options);
    if ($result) {
      $data = json_decode($result->data);
      if ($result->code == 200) {
        return $data;
      }
      else {
        $this->HandleCommonErrors($data);
        return FALSE;
      }
    }
    // We got no data back from the server whatsoever.
    else {
      $data = new stdClass();
      $data->status_code = 500;
      $data->error_description = 'Eventbrite.com servers are not responding.';
      $this->HandleCommonErrors($data);
      return FALSE;
    }
  }


  /**
   * Displays Eventbrite.com error responses to use and logs them in watchdog.
   * @param $data
   */
  private function HandleCommonErrors($data) {
    $values = array(
      '@status_code' => $data->status_code,
      '@error_description' => $data->error_description,
    );
    $message = t('Eventbrite.com returned status @status_code, @error_description', $values);
    drupal_set_message($message, 'error');
    watchdog(WATCHDOG_WARNING, $msg);
  }
}