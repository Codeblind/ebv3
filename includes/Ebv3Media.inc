<?php

/**
 * Created by PhpStorm.
 * User: eriks
 * Date: 8/20/15
 * Time: 11:59 AM
 */
class Ebv3Media extends Ebv3Api {

  public function __construct() {
    parent::__construct();
  }

  /**
   * Get an images using GET /media/:id/ .
   *
   * @param $id
   */
  public function RequestMedia($id) {
    if (is_numeric($id)) {
      $segments = 'media/' . (int) $id;
      $request = '%s/%s/?token=%s';
      $request = sprintf($request, EVENTBRITE_ENDPOINT, $segments, $this->api_token);
      $options = array(
        'method' => 'GET',
      );
      return $this->request($request, $options);
    }
  }

  /**
   * Returns the media item info a format we can use.
   */
  public function GetInfo($id) {
    if ($id) {
      $info = $this->RequestMedia($id);
      $extension = pathinfo($info->url, PATHINFO_EXTENSION);
      $info->extension = $extension;
      return $info;
    }
    else {
      return FALSE;
    }
  }

}