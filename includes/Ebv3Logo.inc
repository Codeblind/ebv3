<?php

/**
 * Created by PhpStorm.
 * User: eriks
 * Date: 8/27/15
 * Time: 1:19 PM
 */
class Ebv3Logo extends Ebv3Media {

  private $save_directory = '';

  public function __construct() {
    parent::__construct();
  }

  /**
   * Requests a file object for the logo. The logo is retrieved from
   * Eventbrite.com, saved locally, and the File API file object returned.
   * If an image is already known to the Drupal File API, it will return the
   * existing local file object.
   *
   * @param $logoid
   *   An Eventbrite logo id.
   * @return
   *  A Drupal File API file object.
   */
  public function LogoFileObject($logoid) {
    if (!$this->save_directory) {
      $error = "A directory must first be set using Ebv3Logo::SetSaveDirectory()";
      throw new Exception($error);
    }
    $fid = $this->DrupalFid($logoid);
    if (!$fid) {
      $file_info = $this->GetInfo($logoid);
      // @todo Make this a configuration variable.
      $file_name = $file_info->id . '.' . $file_info->extension;
      $destination = sprintf('%s/%s', $this->save_directory, $file_name);
      $file = system_retrieve_file($file_info->url, $destination, TRUE, FILE_EXISTS_REPLACE);
    }
    // If the logo id is already in our system, we need to get the local file info.
    else {
      $file = file_load($fid);
    }
    return $file;
  }

  /**
   * Check if a given $logoid is already in the ebv3_event table.
   * @param $logoid
   */
  public function IsUnique($logoid) {
    $sql = 'SELECT nid FROM {ebv3_event} WHERE logoid = :logoid';
    $args = array(
      ':logoid' => $logoid,
    );
    $result = db_query_range($sql,0,1,$args)->fetchCol();
    if (!count($result)) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

  /**
   * Returns the fid associated with a logoid.
   */
  public function DrupalFid($logoid) {
    $sql = 'SELECT fid FROM {ebv3_event} WHERE logoid = :logoid';
    $args = array(
      ':logoid' => $logoid,
    );
    $result = db_query_range($sql,0,1,$args)->fetch();
    if ($result) {
      return $result->fid;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Set the logo save directory path globally.
   * @param $directory
   * @todo Throw exception is $save_directory isn't writable.
   */
  public function SetSaveDirectory($directory) {
    $this->save_directory = $directory;
  }
}