<?php

/**
 * Form builder.
 */
function ebv3_settings_form($form, &$form_state) {

  $form['ebv3_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Your personal OAuth token'),
    '#description' => t(
      'See <a href="@eventbrite">https://www.eventbrite.com/myaccount/apps/</a> for more details.',
      array('@eventbrite' => url('https://www.eventbrite.com/myaccount/apps/'))
    ),
    '#maxlength' => 100,
    '#default_value' => variable_get('ebv3_api_token', ""),
    '#required' => TRUE,
  );

  $form['ebv3_logo_dir'] = array(
    '#type' => 'textfield',
    '#title' => 'Logo save directory',
    '#description' => t('Path to the directory in which to store logo images downloaded from Eventbrite.com. Include stream wrapper, i.e., public://event_logo'),
    '#maxlength' => 100,
    '#default_value' => variable_get('ebv3_logo_dir', "public://events"),
    '#required' => TRUE,
  );

  // TODO! You probably don't need validation or submit handlers if using system_settings_form().
  return system_settings_form($form);
}

/**
 * Form validate handler.
 */
function ebv3_settings_form_validate($form, &$form_state) {

  $el   = 'ebv3_api_token';
  $str = $form_state['values'][$el];
  if (!preg_match('/^[a-zA-Z0-9]+$/', $str)) {
    form_set_error($el, t('API token must be alphanumeric.'));
  }
}

/**
 * Form submit handler.
 */
function ebv3_settings_form_submit($form, &$form_state) {

}