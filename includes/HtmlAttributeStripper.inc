<?php

/**
 * A class for stripping out named attributes from HTML.
 * User: eriks
 * Date: 8/17/15
 * Time: 1:52 PM
 */
class HtmlAttributeStripper {

  /**
   * TRUE if a DOMDocument object has been supplied via the constructor.
   * @var bool
   */
  private $globalDOMDoc = FALSE;

  public function __construct($DOM = "") {
    if (is_object($DOM)) {
      $this->DOM = $DOM;
      $this->globalDOMDoc = TRUE;
    }
    else {
      $this->DOM = new DOMDocument;
    }
  }

  /**
   * Strip attributes using PHP native DOM class. This is liable to have issues
   * when dealing with UTF-8 because for some reason PHP has a long tradition of
   * choking on UTF-8.
   * @param $html
   * @param array $strip
   */
  public function StripAttributesDOM($html, $strip = array()) {
    libxml_use_internal_errors(TRUE);
    $html = $this->InsertXMLPrefix($html);
    $this->DOM->loadHTML($html);
    $this->RemoveXMLPrefix();
    libxml_use_internal_errors(FALSE);
    $xpath = new DOMXPath($this->DOM);
    foreach ($strip as $attr) {
      $pattern = sprintf('//*[@%s]', $attr);
      $nodes = $xpath->query($pattern);
      foreach ($nodes as $node) {
        $node->removeAttribute($attr);
      }
    }
    return $this->DOM->saveHTML();
  }


  /**
   * Prepends '<?xml encoding="utf-8" ?>' to $string.
   * This is a hack to get around issues with PHP's DOMDocument's handling of UTF-8.
   * There may be a better way to do it now.
   */
  private function InsertXMLPrefix($string) {
    return '<?xml encoding="utf-8" ?>' . $string;
  }

  /**
   * Removes the XML prefix set when using CharEncodeXMLPrefix() kludge.
   */
  private function RemoveXMLPrefix() {
    foreach ($this->DOM->childNodes as $item) {
      if ($item->nodeType == XML_PI_NODE) {
        $this->DOM->removeChild($item);
        break;
      }
    }
  }
}