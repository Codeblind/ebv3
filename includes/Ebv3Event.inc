<?php
/**
 * Provides functions for working with events.
 *
 */
class Ebv3Event extends Ebv3Api {

  /**
   * @param $uid
   *  Set the uid as found in the global $user object.
   */
  public function __construct($uid, Ebv3Logo $Logo) {
    parent::__construct();
    $this->uid = $uid;
    $this->Logo = $Logo;
  }


  /**
   * An API query to get all events owned by the account holder.
   */
  public function GetOwnedEvents() {
    // Get ALL attendees
    //https://www.eventbriteapi.com/v3/users/me/owned_event_attendees/?token=SWHYAGXOIYUD7IR4C7YB
    // Get ALL events
    // https://www.eventbriteapi.com/v3/users/me/owned_events/?token=SWHYAGXOIYUD7IR4C7YB
    $fragments = 'users/me/owned_events';
    $request = '%s/%s/?token=%s';
    $request = sprintf($request, EVENTBRITE_ENDPOINT, $fragments, $this->api_token);
    $options = array(
      'method' => 'GET',
    );
    return $this->request($request, $options);
  }

  /**
   * Gets nid associated with eid. We can also use this to check if
   * an event is new, because new events won't map to a nid.
   * @param $eid
   * @return bool
   */
  public function eid_nid($eid) {
    $sql = "SELECT nid FROM {ebv3_event} WHERE eid = :eid";
    $result = db_query($sql, array(':eid' => $eid))->fetchField();
    if ($result > 1) {
      return $result;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks the remote changed timestamps to determine if an existing event
   * has been edited on Eventbrite.com.
   * @param $event
   *  A prepared object of event details.
   * @return bool
   */
  public function remote_changed(&$event) {
    $sql = "SELECT remote_changed FROM {ebv3_event} WHERE eid = :eid";
    $result = db_query($sql, array(':eid' => $event->event_id))->fetchField();
    if ($result !== $event->remote_changed) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Creates an EBV3 event node.
   * @param $data Object
   * @param $user Object
   */
  function save(&$data) {
    $nid = $this->eid_nid($data->event_id);
    if (!$nid) {
      // Only import live events.
      if (in_array($data->status, array('live','completed'))) {
        $this->create($data);
      }
    }
    else {
      $this->update($data, $nid);
    }
  }

  /**
   *  Creates a new Eventbrite node.
   */
  function create($data) {
    try {
      // Create a new node.
      $node = entity_create('node', array('type' => 'ebv3_event'));
      $node->uid = $this->uid;
      $wrapper = entity_metadata_wrapper('node', $node);
      $wrapper->title = $data->title;
      $wrapper->body->set(array('value' => $data->body, 'format' => 'full_html'));
      $wrapper->field_ebv3_event_id->set($data->event_id);
      $wrapper->field_ebv3_event_date->set(array(
        'value' => $data->start,
        'value2' => $data->end,
      ));
      $wrapper->field_ebv3_event_url->set(array('url' => $data->url));

      // Get the logo.
      if ($data->logo->id) {
        $file = $this->Logo->LogoFileObject($data->logo->id);
      }
      if ($file) {
        $wrapper->field_ebv3_event_image->file->set($file);
        $fid = $file->fid;
      }

      // Save our work.
      $wrapper->save();

      // Update the Ebv3 Event table.
      $timestamp = time();
      $fields = array(
        'eid' => $data->event_id,
        'nid' => $node->nid,
        'title' => $data->title,
        'logoid' => $data->logo->id,
        'fid' => ($fid) ? $fid : '',
        'created' => $timestamp,
        'changed' => $timestamp,
        'remote_changed' => $data->remote_changed,
      );
      db_insert('ebv3_event')->fields($fields)->execute();

      // Log successful event.
      $msg = t("Created new Eventbrite event: !title", array('!title'=> $data->title));
      drupal_set_message($msg, 'status');
      watchdog(WATCHDOG_INFO, $msg);
    }
    catch (Exception $e) {
      // @todo It seems possible an event might fail to save without throwing an exception!
      // @todo This may be throwing false errors.
      $msg = t("Failed to create Eventbrite event !event_id: !title.",
                array('!event_id' => $data->event_id, '!title' => $data->title));
      drupal_set_message($msg, 'error');
      watchdog_exception(WATCHDOG_WARNING, $e, $msg);
    }
  }

  /**
   *  Updates an existing Eventbrite node.
   */
  function update($data, $nid) {
    // If the event information has not changed, we
    // don't need to update it!
    if (!$this->remote_changed($data)) {
      return FALSE;
    }
    // If the status is no longer 'live', delete the node.
    // @todo Change this to just unpublish the node and a second process can run cleanup.
    if ('live' !== $data->status) {
      node_delete($nid);
      // Log successful event.
      $msg = t("Deleted unpublished Eventbrite event: !title", array('!title'=> $data->title));
      drupal_set_message($msg, 'status');
      watchdog(WATCHDOG_INFO, $msg);
      return;
    }

    try {
      // Load the entity
      $entities = entity_load('node', array($nid));
      $node = $entities[$nid];;
      $node->uid = $this->uid;
      $wrapper = entity_metadata_wrapper('node', $node);
      $wrapper->title = $data->title;
      $wrapper->body->set(array('value' => $data->body, 'format' => 'full_html'));
      $wrapper->field_ebv3_event_id->set($data->event_id);
      $wrapper->field_ebv3_event_date->set(array(
        'value' => $data->start,
        'value2' => $data->end,
      ));
      $wrapper->field_ebv3_event_url->set(array('value' => $data->url));
      $wrapper->save();

      // Update the Ebv3 Event table.
      $fields = array(
        'changed'        => time(),
        'remote_changed' => $data->remote_changed,
      );
      db_update('ebv3_event')->fields($fields)
        ->condition('nid', $nid, '=')
        ->execute();

      // Log successful event.
      $msg = t("Updated Eventbrite event: !title", array('!title'=> $data->title));
      drupal_set_message($msg, 'status');
      watchdog(WATCHDOG_INFO, $msg);
    }
    catch (Exception $e) {
      // @TODO It seems possible an event might fail to save without throwing an exception!
      $msg = t("Failed to update Eventbrite event.");
      drupal_set_message($msg, 'error');
      watchdog_exception(WATCHDOG_WARNING, $e, $msg);
    }
  }

  /**
   * Prepare an event's data to be worked on.
   * @TODO Make me my own class.
   *
   */
  function event_data_prepare($event) {
    $prepared = new stdClass();
    $prepared->event_id = check_plain($event->id);
    $prepared->title = check_plain($event->name->text);
    // Logo information for this event.
    $prepared->logo = new stdClass();
    $prepared->logo->id = check_plain($event->logo->id);
    $prepared->logo->url = "";
    if (valid_url($event->logo->url, TRUE)) {
      $prepared->logo->url = $event->logo->url;
    }
    // @todo Needs security review.
    // It's impractical to check this legal html here, put the input formatter should do that.
    if (isset($event->description->text)) {
      $strip = array('style', 'id');
      //$prepared->body = $this->htmlAttributeStripper->StripAttributesDOM($event->description->html, $strip);
      $prepared->body = $event->description->html;
    }
    else {
      $prepared->body = 'No description.';
    }
    $prepared->url = "";
    if (valid_url($event->url, TRUE)) {
      $prepared->url = $event->url;
    }
    $prepared->status = check_plain($event->status);
    // Eventbrite timestamps look like this Y-m-d\TH:i:s
    $prepared->start = check_plain($event->start->local);
    $prepared->end  = check_plain($event->end->local);
    $prepared->remote_changed = check_plain($event->changed);
    return $prepared;
  }

  /**
   * Bulk import all events.
   */
  function bulk_import() {
    $data = $this->GetOwnedEvents();
    if ($data) {
      foreach ($data->events as $event) {
        $node = $this->event_data_prepare($event);
        $this->save($node);
        unset($node);
      }
      return "Event import complete.";
    }
    else {
      return "Could not import event data.";
    }

  }
}